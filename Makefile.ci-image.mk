ALPINE_VERSION ?= 3.14
GCLOUD_SDK_VERSION ?= 351.0.0
KUBECTL_VERSION ?= v1.21.3
# See more: https://tanka.dev/
TANKA_VERSION ?= v0.17.1
# See more: https://github.com/jsonnet-bundler/jsonnet-bundler/
JSONNET_BUNDLER_VERSION ?= 65eeb986a83d42e2f754053fbbf075fb65166aac

CI_IMAGE ?= ci-runners-monitoring/ci:dev
CI_IMAGE_LATEST ?= ci-runners-monitoring/ci:latest

.PHONY: pull_ci_image_latest
pull_ci_image_latest:
	# Pull $(CI_IMAGE_LATEST) image
ifneq ($(CI_REGISTRY),)
	@docker login --username $(CI_REGISTRY_USER) --password $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)
	@docker pull $(CI_IMAGE_LATEST) || echo "$(CI_IMAGE_LATEST) is not available; will not use the layer cache"
	@docker logout $(CI_REGISTRY)
else
	# No CI_REGISTRY value; skipping image pulling
endif

.PHONY: build_ci_image
build_ci_image:
	# Build $(CI_IMAGE) image
	@docker build \
		--tag $(CI_IMAGE) \
		--cache-from $(CI_IMAGE_LATEST) \
		--build-arg ALPINE_VERSION=$(ALPINE_VERSION) \
		--build-arg GCLOUD_SDK_VERSION=$(GCLOUD_SDK_VERSION) \
		--build-arg KUBECTL_VERSION=$(KUBECTL_VERSION) \
		--build-arg TANKA_VERSION=$(TANKA_VERSION) \
		--build-arg JSONNET_BUNDLER_VERSION=$(JSONNET_BUNDLER_VERSION) \
		-f ./dockerfiles/ci/Dockerfile \
		./dockerfiles/ci/
	@docker tag $(CI_IMAGE) $(CI_IMAGE_LATEST)

.PHONY: publish_ci_image
publish_ci_image:
	# Publish $(CI_IMAGE) image
	# Publish $(CI_IMAGE_LATEST) image
ifneq ($(CI_REGISTRY),)
	@docker login --username $(CI_REGISTRY_USER) --password $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)
	@docker push $(CI_IMAGE)
	@docker push $(CI_IMAGE_LATEST)
	@docker logout $(CI_REGISTRY)
else
	# No CI_REGISTRY value; skipping image publication
endif
