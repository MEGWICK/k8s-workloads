local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';

local envVar = k.core.v1.envVar;
local secret = k.core.v1.secret;
local volume = k.core.v1.volume;
local volumeProjection = k.core.v1.volumeProjection;
local volumeMount = k.core.v1.volumeMount;

local gceCredentials = {
  volumeName: 'gce-credentials',
  volumePath: '/gce-credentials',
  secretKey: 'gce-credentials',
  secretName: 'gce-credentials',
};

{
  nodeExporterForK3D: {
    'node-exporter-daemonset'+: {
      spec+: {
        template+: {
          spec+: {
            local vmOverride = { mountPropagation: null },
            local neOverride = {
              volumeMounts: [
                std.prune(super.volumeMounts[0] + vmOverride),
                std.prune(super.volumeMounts[1] + vmOverride),
              ],
            },
            containers: [
              super.containers[0] + neOverride,
            ] + super.containers[1:],
          },
        },
      },
    },
  },
  gceCredentials(credentials): {
    gceCredentialsSecret:
      secret.new(
        gceCredentials.secretName,
        {
          [gceCredentials.secretKey]: credentials,
        }
      ),
    'prometheus-prometheus'+: {
      spec+: {
        containers+: [
          {
            name: 'prometheus',
            env: [
              envVar.withName('GOOGLE_APPLICATION_CREDENTIALS') +
              envVar.withValue(std.join('/', [gceCredentials.volumePath, gceCredentials.secretKey])),
            ],
            volumeMounts: [
              volumeMount.withName(gceCredentials.volumeName) +
              volumeMount.withMountPath(gceCredentials.volumePath),
            ],
          },
        ],
        volumes+: [
          volume.withName(gceCredentials.volumeName) +
          volume.projected.withSources([
            volumeProjection.secret.withName(gceCredentials.secretName),
          ]),
        ],
      },
    },
  },
}
