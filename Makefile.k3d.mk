LB_LOCAL_HOST ?= 127.0.0.11

K3D_CLUSTER_NAME := ci-runners-monitoring
KUBECONFIG := $(HOME)/.kube/config

.PHONY: k3d-cluster-create
k3d-cluster-create: SERVERS_COUNT ?= 1
k3d-cluster-create: AGENTS_COUNT ?= 5
k3d-cluster-create:
	@k3d cluster create $(K3D_CLUSTER_NAME) \
		--servers $(SERVERS_COUNT) \
		--agents $(AGENTS_COUNT) \
		--api-port $(LB_LOCAL_HOST):6443 \
		--k3s-server-arg --no-deploy=traefik \
		--port $(LB_LOCAL_HOST):80:80@loadbalancer \
		--port $(LB_LOCAL_HOST):443:443@loadbalancer \
		--port $(LB_LOCAL_HOST):10901:10901@loadbalancer \
		--port $(LB_LOCAL_HOST):10903:10903@loadbalancer \
		--subnet=auto
	@kubectl cluster-info

.PHONY: k3d-cluster-delete
k3d-cluster-delete:
	@k3d cluster delete $(K3D_CLUSTER_NAME)

.PHONY: k3d-cluster-recreate
k3d-cluster-recreate: k3d-cluster-delete k3d-cluster-create

.PHONY: k3d-cluster-start
k3d-cluster-start:
	@k3d cluster start $(K3D_CLUSTER_NAME)
	@kubectl cluster-info

.PHONY: k3d-cluster-stop
k3d-cluster-stop:
	@k3d cluster stop $(K3D_CLUSTER_NAME)

.PHONY: k3d-cluster-restart
k3d-cluster-restart: k3d-cluster-stop k3d-cluster-start
