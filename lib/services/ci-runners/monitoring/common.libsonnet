local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';

local secret = k.core.v1.secret;

function(config)
  assert std.isObject(config);

  {
    'thanos-storage-config-secret':
      secret.new(
        config.thanos.objectStorage.name,
        {
          [config.thanos.objectStorage.key]: std.base64(std.manifestYamlDoc({
            type: 'GCS',
            config: {
              bucket: config.thanos.objectStorage.bucket,
              service_account: config.thanos.objectStorage.serviceAccount,
            },
          })),
        }
      ),
  }
