local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';
local tfResources = import 'gitlab.com/maczukin.dev/k8s/modules/traefik/resources.libsonnet';

local container = k.core.v1.container;
local containerPort = k.core.v1.containerPort;
local deployment = k.apps.v1.deployment;
local envVar = k.core.v1.envVar;
local secret = k.core.v1.secret;
local service = k.core.v1.service;
local servicePort = k.core.v1.servicePort;
local volume = k.core.v1.volume;

local middleware = tfResources.middleware;

local default = {
  environment: error 'must provide environmetn',
  namespace: error 'must provide namespace',
  forwardAuth: {
    local s = self,
    middlewareName: error 'must provide forwardAuth.middlewareName',
    image: 'thomseddon/traefik-forward-auth:2.2.0',
    name: 'traefik-forward-auth',
    port: 4181,
    insecureCookie: false,
    labels: {
      'app.kubernetes.io/name': 'traefik-forward-auth',
      'app.kubernetes.io/component': 'authentication-service',
    },
    secret: {
      name: '%s-secrets' % s.name,
      data: {
        googleClientID: error 'must provide forwardAuth.secret.data.googleClientID',
        googleClientSecret: error 'must provide forwardAuth.secret.data.googleClientSecret',
        secret: error 'must provide forwardAuth.secret.data.secret',
      },
    },
  },
};

function(params)
  local config = default + params;
  assert std.isObject(config);

  {
    secret: secret.new(config.forwardAuth.secret.name, {
      googleClientID: std.base64(config.forwardAuth.secret.data.googleClientID),
      googleClientSecret: std.base64(config.forwardAuth.secret.data.googleClientSecret),
      secret: std.base64(config.forwardAuth.secret.data.secret),
    }),

    deployment:
      local containers = [
        container.new(config.forwardAuth.name, config.forwardAuth.image) +
        container.withArgs(
          [
            '--log-level=info',
            '--log-format=json',
            '--domain=gitlab.com',
            '--cookie-domain=%s.ci-runners.gitlab.net' % config.environment,
          ] +
          if config.forwardAuth.insecureCookie then ['--insecure-cookie'] else []
        ) +
        container.withPorts([
          containerPort.withContainerPort(config.forwardAuth.port) +
          containerPort.withProtocol('TCP'),
        ]) +
        container.withEnv([
          envVar.withName('PROVIDERS_GOOGLE_CLIENT_ID') +
          envVar.valueFrom.secretKeyRef.withName(config.forwardAuth.secret.name) +
          envVar.valueFrom.secretKeyRef.withKey('googleClientID'),

          envVar.withName('PROVIDERS_GOOGLE_CLIENT_SECRET') +
          envVar.valueFrom.secretKeyRef.withName(config.forwardAuth.secret.name) +
          envVar.valueFrom.secretKeyRef.withKey('googleClientSecret'),

          envVar.withName('SECRET') +
          envVar.valueFrom.secretKeyRef.withName(config.forwardAuth.secret.name) +
          envVar.valueFrom.secretKeyRef.withKey('secret'),
        ]),
      ];

      deployment.new(
        name=config.forwardAuth.name,
        replicas=1,
        podLabels=config.forwardAuth.labels,
        containers=containers,
      ) +
      deployment.metadata.withNamespace(config.namespace) +
      deployment.metadata.withLabels(config.forwardAuth.labels) +
      deployment.spec.template.metadata.withNamespace(config.namespace) +
      deployment.spec.template.spec.withTerminationGracePeriodSeconds(60) +
      deployment.spec.template.spec.withVolumes([
        volume.fromSecret(config.forwardAuth.secret.name, config.forwardAuth.secret.name),
      ])
    ,

    service:
      local ports = [
        servicePort.withName('http') +
        servicePort.withPort(config.forwardAuth.port) +
        servicePort.withTargetPort(config.forwardAuth.port),
      ];

      service.new(config.forwardAuth.name, config.forwardAuth.labels, ports) +
      service.metadata.withNamespace(config.namespace) +
      service.metadata.withLabels(config.forwardAuth.labels) +
      service.spec.withType('ClusterIP'),

    middleware:
      middleware.new('traefik-forward-auth') +
      middleware.withSpec({
        forwardAuth: {
          address: 'http://%s:%d' % [config.forwardAuth.name, config.forwardAuth.port],
          authResponseHeaders: [
            'X-Forwarded-User',
          ],
        },
      }),
  }
