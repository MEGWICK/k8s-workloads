function(params)
  local default = {
    namespace: error 'must provide namespace',
  };
  local config = default + params;
  assert std.isObject(config);

  {
    'prometheus-rules-custom':
      {
        apiVersion: 'monitoring.coreos.com/v1',
        kind: 'PrometheusRule',
        metadata: {
          name: 'custom-rules',
          namespace: config.namespace,
          labels: {
            prometheus: 'k8s',
            role: 'alert-rules',
          },
        },
        spec: {
          groups: [
            {
              name: 'CPU rules',
              interval: '1m',
              rules: [
                {
                  record: 'instance_cpu:node_cpu_seconds_not_idle:rate1m',
                  expr: |||
                    sum without (mode) (
                      1 - rate(node_cpu_seconds_total{mode="idle"}[1m])
                    )
                  |||,
                },
                {
                  record: 'instance:node_cpu_utilization:ratio',
                  expr: |||
                    avg without (cpu) (
                      instance_cpu:node_cpu_seconds_not_idle:rate1m
                    )
                  |||,
                },
              ],
            },
            {
              name: 'Node memory',
              rules: [
                {
                  record: 'instance:node_memory_available:ratio',
                  expr: |||
                    (
                      node_memory_MemAvailable_bytes or
                      (
                        node_memory_Buffers_bytes +
                        node_memory_Cached_bytes +
                        node_memory_MemFree_bytes +
                        node_memory_Slab_bytes
                      )
                    ) /
                    node_memory_MemTotal_bytes
                  |||,
                },
                {
                  record: 'instance:node_memory_utilization:ratio',
                  expr: '1 - instance:node_memory_available:ratio',
                },
              ],
            },
            {
              name: 'Node filesystem rules',
              rules: [
                {
                  record: 'instance:node_filesystem_avail:ratio',
                  expr: |||
                    node_filesystem_avail_bytes{device=~"(/dev/.+|tank/dataset)"}
                    /
                    node_filesystem_size_bytes{device=~"(/dev/.+|tank/dataset)"}
                  |||,
                },
                {
                  record: 'instance:node_disk_writes_completed:irate1m',
                  expr: 'sum without(device) (irate(node_disk_writes_completed_total{device=~"sd.*"}[1m]))',
                },
                {
                  record: 'instance:node_disk_reads_completed:irate1m',
                  expr: 'sum without(device) (irate(node_disk_reads_completed_total{device=~"sd.*"}[1m]))',
                },
              ],
            },
          ],
        },
      },
  }
