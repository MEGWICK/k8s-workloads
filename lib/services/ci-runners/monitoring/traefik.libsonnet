local entrypoint = import 'gitlab.com/maczukin.dev/k8s/modules/traefik/entrypoint.libsonnet';
local tfResources = import 'gitlab.com/maczukin.dev/k8s/modules/traefik/resources.libsonnet';
local traefik = import 'gitlab.com/maczukin.dev/k8s/modules/traefik/traefik.libsonnet';
local traefikForwardAuth = import 'traefik-forward-auth.libsonnet';

local ingressRoute = tfResources.ingressRoute;
local ingressRouteTCP = tfResources.ingressRouteTCP;
local middleware = tfResources.middleware;

function(params)
  local default = {
    environment: error 'must provide environment',
    namespace: error 'must provide namespace',
    tcpIngresses: error 'must provide tcpIngresses',
    traefikOverrides: {},
    loadBalancer: {
      IP: error 'must provide loadBalancer.IP',
      sourceRanges: error 'must provide loadBalancer.sourceRanges',
    },
    forwardAuth+: {
      middlewareName: 'traefik-forward-auth',
    },
  };
  local config = default + params;
  assert std.isObject(config);

  local t = traefik(
    {
      namespace: config.namespace,
      entrypoints:
        [
          local ingress = config.tcpIngresses[ingressName];

          entrypoint.new(ingress.entrypoint, ingress.port) +
          entrypoint.withServiceOverride({
            spec+: {
              loadBalancerIP: config.loadBalancer.IP,
              loadBalancerSourceRanges: config.loadBalancer.sourceRanges,
            },
          })

          for ingressName in std.objectFields(config.tcpIngresses)
        ] +
        [
          entrypoint.new('web', 80) +
          entrypoint.withRedirections({
            entrypoint: {
              to: 'websecure',
            },
          }) +
          entrypoint.withServiceOverride({
            spec+: {
              loadBalancerIP: config.loadBalancer.IP,
            },
          }),

          entrypoint.new('websecure', 443) +
          entrypoint.withCertResolver('acme') +
          entrypoint.withServiceOverride({
            spec+: {
              loadBalancerIP: config.loadBalancer.IP,
            },
          }),
        ],
      dashboard+: {
        insecure: false,
      },
      acme+: {
        enabled: true,
        email: 'tomasz@gitlab.com',
        tlsChallenge: {
          enabled: true,
        },
        storage+: {
          pvc+: {
            name: 'ci-runners-traefik-acme',
            storageClassName: 'standard',
            request: {
              storage: '250Mi',
            },
          },
        },
      },
    } +
    config.traefikOverrides
  );

  {
    'traefik-crd': t.crd,
  } +
  if !config.initializeOnly then
    {
      'traefik-rbac': t.rbac,
      'traefik-services': t.services,
      'traefik-deployment': t.deployment,
      'traefik-dashboard': t.dashboard,
      'traefik-forward-auth': traefikForwardAuth(config),
      'traefik-servicemonitor': config._addCommonRelabelConfigs(t.serviceMonitor),
    } +
    {
      ['traefik-ingress-route-tcp-%s' % ingressName]:
        local ingress = config.tcpIngresses[ingressName];

        ingressRouteTCP.new(ingressName) +
        ingressRouteTCP.metadata.withNamespace(config.namespace) +
        ingressRouteTCP.spec.withEntryPoints([ingress.entrypoint]) +
        ingressRouteTCP.spec.withRoutes([
          ingressRouteTCP.route.new('HostSNI(`*`)') +
          ingressRouteTCP.route.withServices([
            ingressRouteTCP.route.service.new(ingress.service) +
            ingressRouteTCP.route.service.withPort(ingress.targetPort),
          ]),
        ])

      for ingressName in std.objectFields(config.tcpIngresses)
    } +
    {
      'traefik-ingress-route-internal-metrics':
        ingressRoute.new('internal-metrics') +
        ingressRoute.metadata.withNamespace(config.namespace) +
        ingressRoute.spec.withEntryPoints('traefik') +
        ingressRoute.spec.withRoutes([
          ingressRoute.route.new('PathPrefix(`/metrics`)') +
          ingressRoute.route.withServices([
            ingressRoute.route.service.new('prometheus@internal') +
            ingressRoute.route.service.withKind('TraefikService'),
          ]),
        ]),

      'traefik-ingress-route-traefik-https':
        ingressRoute.new('traefik') +
        ingressRoute.metadata.withNamespace(config.namespace) +
        ingressRoute.spec.withEntryPoints('websecure') +
        ingressRoute.spec.withRoutes([
          ingressRoute.route.new('Host(`monitoring-lb.%s.ci-runners.gitlab.net`)' % config.environment) +
          ingressRoute.route.withServices([
            ingressRoute.route.service.new('api@internal') +
            ingressRoute.route.service.withKind('TraefikService'),
          ]) +
          ingressRoute.route.withMiddlewares([
            ingressRoute.route.middleware.new(config.forwardAuth.middlewareName) +
            ingressRoute.route.middleware.withNamespace(config.namespace),
          ]),
        ]),

      'traefik-ingress-route-prometheus-https':
        ingressRoute.new('prometheus-k8s-https') +
        ingressRoute.metadata.withNamespace(config.namespace) +
        ingressRoute.spec.withEntryPoints('websecure') +
        ingressRoute.spec.withRoutes([
          ingressRoute.route.new('Host(`prometheus.%s.ci-runners.gitlab.net`)' % config.environment) +
          ingressRoute.route.withServices([
            ingressRoute.route.service.new('prometheus-k8s') +
            ingressRoute.route.service.withPort(9090),
          ]) +
          ingressRoute.route.withMiddlewares([
            ingressRoute.route.middleware.new(config.forwardAuth.middlewareName) +
            ingressRoute.route.middleware.withNamespace(config.namespace),
          ]),
        ]),
    }
  else
    {}
