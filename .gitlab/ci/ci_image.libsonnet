local dind = import 'lib/dind.libsonnet';
local job = import 'lib/job.libsonnet';
local rules = import 'lib/rules.libsonnet';

{
  variables+: {
    CI_IMAGE: '${CI_REGISTRY_IMAGE}/ci:20210820.2',
    CI_IMAGE_LATEST: '${CI_REGISTRY_IMAGE}/ci:latest',
  },
  'prepare ci image':
    job.withStage('prepare') +
    job.withScript([
      'apk add --no-cache make',
      'make pull_ci_image_latest',
      'make build_ci_image',
      'make publish_ci_image',
    ]) +
    job.withRules([
      rules.conditions.ifNonDefaultBranch +
      rules.changes.with([
        '.gitlab/ci/ci_image.libsonnet',
        'Makefile.ci-image.mk',
        'dockerfiles/ci/*',
      ]),
    ]) +
    dind,
}
