local job = import 'lib/job.libsonnet';
local rules = import 'lib/rules.libsonnet';

{
  'jsonnet fmt':
    job.withStage('test') +
    job.withScript([
      'make check-jsonnet-fmt',
    ]),

  'tanka fmt':
    job.withStage('test') +
    job.withScript([
      'make check-tk-fmt',
    ]),

  'gitlab-ci.yml selftest':
    job.withStage('test') +
    job.withScript([
      'make check-gitlab-ci-yml',
    ]),

  'danger review':
    job.withStage('test') +
    job.withNeeds() +
    job.withImage('${CI_REGISTRY}/gitlab-org/gitlab-build-images:danger') +
    job.withScript([
      'danger --fail-on-errors=true',
    ]) +
    job.withRules([
      rules.conditions.ifNonDefaultBranchOnCanonical,
    ]),

  'runner change lock':
    job.withStage('test') +
    job.withNeeds() +
    job.withImage('${CI_REGISTRY}/gitlab-com/gl-infra/change-lock:latest') +
    job.withScript([
      'change-lock --tags=runner --tags=infra --slack-channels=#f_linux_shared_runners',
    ]) +
    job.withRules([
      rules.conditions.ifNonDefaultBranchOnMirror,
    ]),
}
