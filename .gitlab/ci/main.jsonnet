local warning = "# Generated automatically with 'make build-gitlab-ci-yaml' - do not hand edit!\n\n";
local global =
  {
    stages: [
      'prepare',
      'test',
      'plan',
      'apply',
      'check',
    ],
    variables: {
      DOCKER_VERSION: '20.10.2',
    },
    default: {
      image: '${CI_IMAGE}',
      tags: [
        'gitlab-org',
      ],
    },
    workflow: {
      rules: [
        {
          'if': '$CI_COMMIT_TAG',
        },
        {
          'if': '$CI_COMMIT_BRANCH',
        },
      ],
    },
  };

warning + std.manifestYamlDoc(
  global +
  (import 'ci_image.libsonnet') +
  (import 'test.libsonnet') +
  (import 'environments.libsonnet') +
  (import 'check.libsonnet')
)
