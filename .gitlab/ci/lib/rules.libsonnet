{
  local s = self,

  conditions:: {
    ifDefaultBranch: {
      'if': '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH',
    },
    ifDefaultBranchOnMirror: {
      'if': '$MIRROR == "true" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH',
    },
    ifDefaultBranchOnCanonical: {
      'if': '$CANONICAL == "true" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH',
    },
    ifNonDefaultBranch: {
      'if': '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH',
    },
    ifNonDefaultBranchOnMirror: {
      'if': '$MIRROR == "true" && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH',
    },
    ifNonDefaultBranchOnCanonical: {
      'if': '$CANONICAL == "true" && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH',
    },
    ifMirror: {
      'if': '$MIRROR == "true"',
    },
    ifCanonical: {
      'if': '$CANONICAL == "true"',
    },
  },

  changes:: {
    with(changes): { changes+: if std.isArray(changes) then changes else [changes] },
    withEnvironment(environment, component, stage): s.changes.with([
      'environments/%s/%s/%s/*' % [environment, component, stage],
      '.gitlab/ci/environments/%s_%s_%s.libsonnet' % [environment, component, stage],
    ]),
    withServiceComponent(service, component): s.changes.with('lib/services/%s/%s/*' % [service, component]),
  },

  when:: {
    onSuccess: { when: 'on_success' },
    manual: { when: 'manual' },
  },

  default:: [
    s.conditions.ifNonDefaultBranch,
    s.conditions.ifDefaultBranch,
  ],
}
