{
  withStage(stage): { stage: stage },
  withNeeds(needs=[]): { needs+: if std.isArray(needs) then needs else [needs] },
  withImage(image): { image: image },
  withServices(services): { services+: if std.isArray(services) then services else [services] },
  withVariables(variables): { variables+: variables },
  withTags(tags): { tags+: if std.isArray(tags) then tags else [tags] },
  withBeforeScript(beforeScript): { before_script+: if std.isArray(beforeScript) then beforeScript else [beforeScript] },
  withScript(script): { script+: if std.isArray(script) then script else [script] },
  withAllowFailure(allow): { allow_failure: allow },
  withRules(rules): { rules+: if std.isArray(rules) then rules else [rules] },
  environment:: {
    withName(name): { environment+: { name: name } },
  },
}
