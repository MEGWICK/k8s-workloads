local job = import 'job.libsonnet';
local rules = import 'rules.libsonnet';

local tankaChanges(environment, component, stage, changes) =
  rules.changes.withEnvironment(environment, component, stage) +
  rules.changes.with([
    '.gitlab/ci/lib/kube.libsonnet',
  ]) +
  rules.changes.with(changes)
;

local tankaNonDefaultBranchRules(environment, component, stage, changes) =
  rules.conditions.ifNonDefaultBranchOnMirror +
  tankaChanges(environment, component, stage, changes)
;

local tankaDefaultBranchRules(environment, component, stage, changes) =
  rules.conditions.ifDefaultBranchOnMirror +
  tankaChanges(environment, component, stage, changes)
;

{
  local s = self,

  kubectl:: {
    beforeScript: [
      'gcloud auth activate-service-account --key-file=${GOOGLE_APPLICATION_CREDENTIALS}',
      'gcloud config set project ${GCP_PROJECT_ID}',
      'gcloud container clusters get-credentials ${GKE_CLUSTER_NAME} --zone=${GKE_ZONE}',
    ],
  },
  tanka:: {
    dev: {
      beforeScript: ['jb install'],
    },
    prod: {
      beforeScript:
        s.kubectl.beforeScript +
        ['jb install'],
    },
  },

  plan:: {
    dev(environment, component, tankaFlags=[], changes=[], preScript=[], postScript=[]):
      job.withStage('plan') +
      job.withBeforeScript(s.tanka.dev.beforeScript) +
      job.withScript(
        preScript +
        ['tk show environments/%s/%s/dev %s' % [environment, component, std.join(' ', tankaFlags)]] +
        postScript
      ) +
      job.withRules([
        tankaNonDefaultBranchRules(environment, component, 'dev', changes) +
        rules.when.onSuccess,
      ]),

    prod(environment, component, tankaFlags=[], changes=[], preScript=[], postScript=[]):
      job.withStage('plan') +
      job.environment.withName('%s-%s' % [environment, component]) +
      job.withBeforeScript(s.tanka.prod.beforeScript) +
      job.withScript(
        preScript +
        ['tk diff --with-prune environments/%s/%s/prod --exit-zero %s' % [environment, component, std.join(' ', tankaFlags)]] +
        postScript
      ) +
      job.withRules([
        tankaNonDefaultBranchRules(environment, component, 'prod', changes) +
        rules.when.onSuccess,
        tankaDefaultBranchRules(environment, component, 'prod', changes) +
        rules.when.onSuccess,
      ]),
  },
  apply:: {
    prod(environment, component, tankaFlags=[], changes=[], preScript=[], postScript=[]):
      job.withStage('apply') +
      job.environment.withName('%s-%s' % [environment, component]) +
      job.withBeforeScript(s.tanka.prod.beforeScript) +
      job.withScript(
        preScript +
        [
          'tk apply environments/%s/%s/prod --dangerous-auto-approve %s' % [environment, component, std.join(' ', tankaFlags)],
          'tk prune environments/%s/%s/prod --dangerous-auto-approve %s' % [environment, component, std.join(' ', tankaFlags)],
        ] +
        postScript
      ) +
      job.withRules([
        tankaDefaultBranchRules(environment, component, 'prod', changes) +
        rules.when.manual,
      ]),
  },
}
