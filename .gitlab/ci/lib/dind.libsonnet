local job = import 'job.libsonnet';

job.withImage('docker:${DOCKER_VERSION}-git') +
job.withServices(['docker:${DOCKER_VERSION}-dind']) +
job.withTags(['gitlab-org-docker']) +
job.withVariables({
  DOCKER_DRIVER: 'overlay2',
  DOCKER_HOST: 'tcp://docker:2376',
  DOCKER_TLS_VERIFY: 1,
  DOCKER_TLS_CERTDIR: '/certs',
  DOCKER_CERT_PATH: '/certs/client',
})
